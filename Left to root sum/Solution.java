/**
 * Definition for binary tree
 * class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) {
 *      val = x;
 *      left=null;
 *      right=null;
 *     }
 * }
 */
public class Solution {
    public ArrayList<ArrayList<Integer>> pathSum(TreeNode A, int B) 
    {
        int sum = 0;
        ArrayList<ArrayList<Integer>> resultPath = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> currentPath = new ArrayList<Integer>();
        this.pathSum(A, B, sum, resultPath, currentPath);
        return resultPath;
    }

    public void pathSum(TreeNode A, int B, int sum, ArrayList<ArrayList<Integer>> resultPath, ArrayList<Integer> currentPath)
    {
        currentPath.add(A.val);
        sum += A.val;
        
        if(sum == B && A.left == null && A.right == null)
        {
            resultPath.add((ArrayList<Integer>)currentPath.clone());
            currentPath.remove(currentPath.size()-1);
            return;
        }
        
        if(A.left != null)
        {
            pathSum(A.left, B, sum, resultPath, currentPath);
        }
        
        if(A.right != null)
        {
            pathSum(A.right, B, sum, resultPath, currentPath);
        }
        
        currentPath.remove(currentPath.size()-1);
    }
}
