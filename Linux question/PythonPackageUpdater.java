import java.io.*;
import java.lang.ProcessBuilder.Redirect;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PythonPackageUpdater
{

	public static void main(String[] args) throws IOException, Exception
	{
		BufferedReader input = new BufferedReader(new FileReader(new File("input.txt")));

		String tempLine;
		StringBuilder tempInputString = new StringBuilder();
		while((tempLine = input.readLine()) != null)
		{
			tempInputString.append(tempLine);
		}
		
		String inputString = tempInputString.toString();
		String pattern = "\\{Dependencies\\s*=\\s*\\{(.*==.*)*\\},\\s+\\}";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(inputString);
		if(m.find())
		{
			String s = m.group(1);
			String[] packageNames = s.split(",");
			ProcessBuilder pb = new ProcessBuilder();
			File output = new File("output.txt");
			File error = new File("error.txt");
			pb.redirectOutput(Redirect.appendTo(output));
			pb.redirectError(Redirect.appendTo(error));
			Process process;
			for(String a : packageNames)
			{
				String packageName = a.trim();
				pb.command("bash","-c","/Library/Frameworks/Python.framework/Versions/3.4/bin/pip3 install "+packageName+" --upgrade");
				process = pb.start();
				int exitValue = process.waitFor();
				if(exitValue != 0)
				{
					System.out.println("***Failed: " +packageName+ " Check the logs.***");
				}
				if(exitValue == 0 || exitValue == 1) //0: Successfully upgraded. 1: Already upgraded.
				{
					System.out.println("Done: "+ packageName);
				}
			}
			
		}
		else
		{
			System.out.println("Uh, Oh! Something is wrong with in the input format.");
		}
		
		input.close();
		
		
	}
}
