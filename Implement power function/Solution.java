public class Solution 
{
	public int pow(int x, int n, int d) 
    {
        long result = 1;
        long x1 = x;
        if(x == 0)
        {
            return 0;
        }
        
        if(n == 0)
        {
            return 1;
        }
        
        if(x1 < 0)
        {
            x1 = (x1+d)%d;
        }
        else
        {
            x1 = x1%d;
        }
        
        while(n > 0)
        {
            if(n%2 != 0)
            {
                result = (result*x1)%d;
            }
                
            n = n/2;
            x1 = (x1*x1)%d;
        }
        
        return (int)result;
    }
}
